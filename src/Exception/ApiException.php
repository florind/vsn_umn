<?php
namespace App\Exception;

use App\Helper\ApiMessage;
use Symfony\Component\HttpFoundation\Response;

abstract class ApiException extends \Exception
{
    protected $message = ApiMessage::RESPONSE_API_ERROR;
    protected $code = Response::HTTP_INTERNAL_SERVER_ERROR;
    private $details = [];

    public function getDetails()
    {
        return $this->details;
    }

    public function setDetails(array $details)
    {
        $this->details = $details;
    }
}