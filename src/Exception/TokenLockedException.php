<?php
namespace App\Exception;

use App\Helper\ApiMessage;
use Symfony\Component\HttpFoundation\Response;

class TokenLockedException extends ApiException
{
    protected $message = ApiMessage::RESPONSE_TOKEN_LOCKED;
    protected $code = Response::HTTP_BAD_REQUEST;
}