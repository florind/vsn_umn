<?php
namespace App\Exception;

use App\Helper\ApiMessage;
use Symfony\Component\HttpFoundation\Response;

class AccessDeniedException extends ApiException
{
    protected $message = ApiMessage::RESPONSE_ACCESS_DENIED;
    protected $code = Response::HTTP_FORBIDDEN;
}