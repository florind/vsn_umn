<?php
namespace App\Exception;

use App\Helper\ApiMessage;
use Symfony\Component\HttpFoundation\Response;

class PasswordResetException extends ApiException
{
    protected $message = ApiMessage::RESPONSE_PASSWORD_RESET_ERROR;
    protected $code = Response::HTTP_BAD_REQUEST;
}