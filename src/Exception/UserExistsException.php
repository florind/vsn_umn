<?php
namespace App\Exception;

use App\Helper\ApiMessage;
use Symfony\Component\HttpFoundation\Response;

class UserExistsException extends ApiException
{
    protected $message = ApiMessage::RESPONSE_USER_EXISTS;
    protected $code = Response::HTTP_BAD_REQUEST;
}