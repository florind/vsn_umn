<?php
namespace App\Exception;

use App\Helper\ApiMessage;
use Symfony\Component\HttpFoundation\Response;

class AccountLockedException extends ApiException
{
    protected $message = ApiMessage::RESPONSE_ACCOUNT_LOCKED;
    protected $code = Response::HTTP_FORBIDDEN;
}