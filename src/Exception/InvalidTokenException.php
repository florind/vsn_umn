<?php
namespace App\Exception;

use App\Helper\ApiMessage;
use Symfony\Component\HttpFoundation\Response;

class InvalidTokenException extends ApiException
{
    protected $message = ApiMessage::RESPONSE_INVALID_TOKEN;
    protected $code = Response::HTTP_BAD_REQUEST;
}