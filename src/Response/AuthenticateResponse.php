<?php
namespace App\Response;

use App\Helper\ApiMessage;

class AuthenticateResponse extends BaseResponse
{
    private $email;
    private $token;
    private $refreshToken;

    public function __construct(
        string $email,
        string $token,
        string $refreshToken
    ) {
        $this->email = $email;
        $this->token = $token;
        $this->refreshToken = $refreshToken;
    }

    function getData()
    {
        return [
            'email' => $this->email,
            'token' => $this->token,
            'refresh_token' => $this->refreshToken
        ];
    }

    function getMessage()
    {
        return ApiMessage::RESPONSE_SUCCESS;
    }
}