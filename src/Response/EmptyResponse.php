<?php
namespace App\Response;

class EmptyResponse extends BaseResponse
{
    private $message;

    public function __construct(string $message)
    {
        $this->message = $message;
    }

    function getData()
    {
        return [];
    }

    function getMessage()
    {
        return $this->message;
    }
}