<?php
namespace App\Response;

use App\Helper\ApiMessage;

class ValidationErrorResponse extends BaseResponse
{
    private $errors;

    public function __construct(array $errors)
    {
        $this->errors = $errors;
    }

    function getData()
    {
        return $this->errors;
    }

    function getMessage()
    {
        return ApiMessage::RESPONSE_VALIDATION_ERROR;
    }

}