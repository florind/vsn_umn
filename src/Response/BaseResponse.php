<?php
namespace App\Response;

abstract class BaseResponse implements \JsonSerializable
{
    abstract function getData();
    abstract function getMessage();

    function jsonSerialize()
    {
        return [
            'response' => $this->getMessage(),
            'data' => $this->getData()
        ];
    }
}