<?php
namespace App\Response;

use App\Helper\ApiMessage;

class IdentityTokenResponse extends BaseResponse
{
    private $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    function getData()
    {
        return ['token' => $this->token];
    }

    function getMessage()
    {
        return ApiMessage::RESPONSE_SUCCESS;
    }
}