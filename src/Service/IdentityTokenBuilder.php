<?php
namespace App\Service;

use Jose\Component\Core\Converter\JsonConverter;
use Jose\Component\Core\JWK;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\Serializer\Serializer;

class IdentityTokenBuilder
{
    const TOKEN_LIFESPAN = 15 * 60;

    private $encoder;
    private $serializer;
    private $key;
    private $builder;
    private $header;

    public function __construct(
        JsonConverter $encoder,
        Serializer $serializer,
        JWK $key,
        JWSBuilder $builder,
        array $header
    ) {
        $this->key = $key;
        $this->header = $header;
        $this->builder = $builder;
        $this->encoder = $encoder;
        $this->serializer = $serializer;
    }

    public function tokenize(array $data): string
    {
        $base = [
            'iat' => time(),
            'nbf' => time(),
            'exp' => time() + static::TOKEN_LIFESPAN,
            'iss' => 'VSN UMN',
            'aud' => 'VSN'
        ];

        $payload = array_merge($base, $data);

        $jws = $this->builder->create()
            ->withPayload($this->encoder->encode($payload))
            ->addSignature($this->key, $this->header)
            ->build();

        return $this->serializer->serialize($jws);
    }
}