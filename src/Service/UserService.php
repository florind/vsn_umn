<?php
namespace App\Service;

use App\DomainEvent\DomainEventPublisher;
use App\DomainEvent\GeneratedUserActivationCodeEvent;
use App\DomainEvent\UserActivatedEvent;
use App\DomainEvent\UserForgotPasswordEvent;
use App\DomainEvent\UserRegisteredEvent;
use App\DomainEvent\UserResetPasswordEvent;
use App\Entity\User;
use App\Exception\AccessDeniedException;
use App\Exception\AccountLockedException;
use App\Exception\InvalidTokenException;
use App\Exception\PasswordResetException;
use App\Exception\UserExistsException;
use App\Repository\UserRepository;
use App\Request\ActivateUserRequest;
use App\Request\AuthenticateRequest;
use App\Request\ForgotPasswordRequest;
use App\Request\GenerateActivationCodeRequest;
use App\Request\IdentityTokenRequest;
use App\Request\PasswordResetRequest;
use App\Request\RegisterRequest;
use App\Response\AuthenticateResponse;
use App\Response\IdentityTokenResponse;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    /** @var  EntityManagerInterface */
    private $em;

    /** @var  UserPasswordEncoderInterface */
    private $passwordEncoder;

    /** @var  DomainEventPublisher */
    private $domainEventPublisher;

    /** @var  UserRepository */
    private $repository;

    /** @var  IdentityTokenBuilder */
    private $identityTokenBuilder;

    public function __construct(
        UserRepository $repository,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $encoder,
        DomainEventPublisher $publisher,
        IdentityTokenBuilder $builder
    ) {
        $this->em = $em;
        $this->repository = $repository;
        $this->passwordEncoder = $encoder;
        $this->identityTokenBuilder = $builder;
        $this->domainEventPublisher = $publisher;
    }

    public function register(RegisterRequest $model)
    {
        $user = User::build($model->getEmail());
        $user->setPassword($this->passwordEncoder->encodePassword($user, $model->getPassword()));

        try {
            $this->em->persist($user);
            $this->em->flush();

            $this->domainEventPublisher->publish(
                new UserRegisteredEvent(
                    $user->getId(),
                    $model->getFirstName(),
                    $model->getLastName(),
                    $model->getBirthday(),
                    $model->getGender(),
                    $model->getLocale(),
                    $user->getToken()->getPlainCode()
                )
            );
        } catch (UniqueConstraintViolationException $ex) {
            throw new UserExistsException();
        }
    }

    public function authenticate(AuthenticateRequest $model): AuthenticateResponse
    {
        try {
            $user = $this->repository->withEmail($model->getEmail());

            if ($user->isLocked()) {
                throw new AccountLockedException();
            }

            if (!$user->isActive()) {
                throw new AccessDeniedException();
            }

            if (!$this->passwordEncoder->isPasswordValid($user, $model->getPassword())){
                throw new AccessDeniedException();
            }

            $token = $this->identityTokenBuilder
                ->tokenize([
                    'pid' => $user->getProfileId(),
                    'roles' => $user->getRoles()
                ]);
            
            return new AuthenticateResponse(
                $user->getEmail(),
                $token,
                $user->getRefreshToken()
            );
        } catch (NoResultException $ex) {
            throw new AccessDeniedException();
        }
    }

    public function generateIdentityToken(IdentityTokenRequest $model): IdentityTokenResponse
    {
        try {
            $user = $this->repository->withEmail($model->getEmail());

            if (!$user->isActive() || $user->getRefreshToken() !== $model->getRefreshToken()) {
                throw new AccessDeniedException();
            }

            return new IdentityTokenResponse(
                $this->identityTokenBuilder
                    ->tokenize([
                        'pid' => $user->getProfileId(),
                        'roles' => $user->getRoles()
                    ])
            );
        } catch (NoResultException $ex) {
            throw new AccessDeniedException();
        }
    }

    public function activate(ActivateUserRequest $model)
    {
        try {
            $user = $this->repository->withEmail($model->getEmail());

            $user->activate($model->getToken());
            $this->em->flush();

            $this->domainEventPublisher->publish(
                new UserActivatedEvent(
                    $user->getEmail(),
                    $model->getLocale()
                )
            );
        } catch (NoResultException $ex) {
            throw new InvalidTokenException();
        } catch (InvalidTokenException $exception) {
            $this->em->flush();
            throw $exception;
        }
    }

    public function resendActivationCode(GenerateActivationCodeRequest $model)
    {
        try {
            $user = $this->repository->withEmail($model->getEmail());
            $user->refreshActivationToken();
            $this->em->flush();

            $this->domainEventPublisher->publish(
                new GeneratedUserActivationCodeEvent(
                    $user->getEmail(),
                    $user->getToken()->getPlainCode(),
                    $model->getLocale()
                )
            );
        } catch (NoResultException $ex) {
            throw new AccessDeniedException();
        }
    }

    public function forgotPassword(ForgotPasswordRequest $model)
    {
        try {
            $user = $this->repository->withEmail($model->getEmail());

            $user->forgotPassword();
            $this->em->flush();

            $this->domainEventPublisher->publish(
                new UserForgotPasswordEvent(
                    $user->getEmail(),
                    $user->getToken()->getPlainCode(),
                    $model->getLocale()
                )
            );
        } catch (NoResultException $ex) {
            return;
        }
    }

    public function resetPassword(PasswordResetRequest $model)
    {
        try {
            $user = $this->repository->withEmail($model->getEmail());

            $user->resetPassword(
                $model->getToken(),
                $this->passwordEncoder->encodePassword($user, $model->getPassword())
            );
            $this->em->flush();

           $this->domainEventPublisher->publish(
               new UserResetPasswordEvent(
                   $user->getEmail(),
                   $model->getLocale()
               )
           );
        } catch (NoResultException $ex) {
            throw new PasswordResetException();
        } finally {
            $this->em->flush();
        }
    }
}