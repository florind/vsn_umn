<?php
namespace App\Consumer;

use App\DomainEvent\DomainEventPublisher;
use App\DomainEvent\UserCreatedEvent;
use App\DomainEvent\UserRegisteredEvent;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class UserRegisteredEventConsumer implements ConsumerInterface
{
    private $repository;
    private $config;
    private $client;
    private $em;
    private $eventPublisher;

    public function __construct(
        UserRepository $repository,
        array $asnEndpointConfig,
        EntityManagerInterface $em,
        DomainEventPublisher $eventPublisher
    ) {
        $this->em = $em;
        $this->repository = $repository;
        $this->config = $asnEndpointConfig;
        $this->eventPublisher = $eventPublisher;

        $this->client = new Client([
            'base_uri' => $asnEndpointConfig['base_uri'],
            'verify' => false,
            'headers' => ['Authorization' => $this->generateBearerToken($asnEndpointConfig['api_key'])]
        ]);
    }

    public function execute(AMQPMessage $msg)
    {
        try {
            $event = UserRegisteredEvent::fromJson($msg->getBody());
            /** @var User $user */
            $user = $this->repository->find($event->getId());
            if (null === $user) { return; }

            $response = $this->client->post($this->config['resources']['profile'], [
                'form_params' => [
                    'first_name' => $event->getFirstName(),
                    'last_name' => $event->getLastName(),
                    'birthday' => $event->getBirthday(),
                    'gender' => $event->getGender(),
                    'id' => $user->getId()
                ]
            ]);

            $content = json_decode($response->getBody()->getContents(), true);
            $profileId = $content['hash'];

            $user->setProfileId($profileId);
            $this->em->flush();

            $this->eventPublisher->publish(new UserCreatedEvent(
                $user->getEmail(),
                $event->getToken(),
                $event->getLocale()
            ));
        } catch (ClientException $ex) {
            echo $ex->getMessage();
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
    }

    private function generateBearerToken($apiKey)
    {
        return sprintf(
            "Bearer %s",
            base64_encode(sprintf('%s %s', $apiKey, '*'))
        );
    }
}