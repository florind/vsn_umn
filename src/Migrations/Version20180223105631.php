<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180223105631 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX token_idx ON user');
        $this->addSql('ALTER TABLE user ADD token_scope VARCHAR(255) DEFAULT NULL, CHANGE token token_code VARCHAR(255) DEFAULT NULL, CHANGE profile_id token_attempts INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649151105FA ON user (token_code)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_8D93D649151105FA ON user');
        $this->addSql('ALTER TABLE user ADD token VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, DROP token_code, DROP token_scope, CHANGE token_attempts profile_id INT DEFAULT NULL');
        $this->addSql('CREATE INDEX token_idx ON user (email, token)');
    }
}
