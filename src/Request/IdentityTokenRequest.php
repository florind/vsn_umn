<?php
namespace App\Request;

use App\Helper\ApiMessage;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class IdentityTokenRequest
{
    /**
     * @var string
     * @Assert\NotBlank(message=ApiMessage::ERROR_EMPTY)
     * @Assert\Email(
     *     checkMX=true,
     *     message=ApiMessage::ERROR_VALUE_INVALID
     * )
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(message=ApiMessage::ERROR_EMPTY)
     */
    private $refreshToken;

    public function __construct(Request $request)
    {
        $this->email = $request->get('email', null);
        $this->refreshToken = $request->get('refresh_token', null);
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }
}