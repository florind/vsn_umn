<?php
namespace App\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use App\Helper\ApiMessage;

class RegisterRequest
{
    /**
     * @var string
     * @Assert\NotBlank(message = ApiMessage::ERROR_EMPTY)
     * @Assert\Email(
     *     checkMX = true,
     *     message = ApiMessage::ERROR_VALUE_INVALID
     * )
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(message=ApiMessage::ERROR_EMPTY)
     * @Assert\Regex(
     *     pattern = "/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/",
     *     htmlPattern = false,
     *     message = ApiMessage::ERROR_VALUE_INVALID
     * )
     */
    private $password;

    /**
     * @var string
     * @Assert\NotBlank(message = ApiMessage::ERROR_EMPTY)
     * @Assert\Regex(
     *     pattern = "/[a-zA-Z]+/",
     *     htmlPattern = false,
     *     message = ApiMessage::ERROR_VALUE_INVALID
     * )
     * @Assert\Length(
     *     min = 2,
     *     max = 100,
     *     minMessage = ApiMessage::ERROR_VALUE_LENGTH,
     *     maxMessage = ApiMessage::ERROR_VALUE_LENGTH,
     * )
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotBlank(message = ApiMessage::ERROR_EMPTY)
     * @Assert\Regex(
     *     pattern = "/[a-zA-Z]+/",
     *     htmlPattern = false,
     *     message = ApiMessage::ERROR_VALUE_INVALID
     * )
     * @Assert\Length(
     *     min = 2,
     *     max = 100,
     *     minMessage = ApiMessage::ERROR_VALUE_LENGTH,
     *     maxMessage = ApiMessage::ERROR_VALUE_LENGTH,
     * )
     */
    private $lastName;

    /**
     * @var string
     * @Assert\NotBlank(message=ApiMessage::ERROR_EMPTY)
     * @Assert\Choice(
     *     choices = {"male", "female"},
     *     message = ApiMessage::ERROR_VALUE_INVALID
     * )
     */
    private $gender;

    /**
     * @var string
     * @Assert\NotBlank(message=ApiMessage::ERROR_EMPTY)
     */
    private $birthday;

    private $locale;

    public function __construct(Request $request)
    {
        $this->email = $request->get('email', null);
        $this->password = $request->get('pass', null);
        $this->firstName = $request->get('first_name', null);
        $this->lastName = $request->get('last_name', null);
        $this->gender = $request->get('gender', null);
        $this->birthday = $request->get('birthday', null);
        $this->locale = $request->get('locale', 'en');
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function getBirthday(): string
    {
        return $this->birthday;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        $birthday = \DateTime::createFromFormat('Y-m-d', $this->birthday);

        if (empty($birthday)) {
            $context->buildViolation(ApiMessage::ERROR_VALUE_INVALID)
                ->atPath('birthday')
                ->addViolation();

            return;
        }

        $today = new \DateTime();

        if ($today->diff($birthday)->y <= 16) {
            $context->buildViolation(ApiMessage::ERROR_VALUE_INVALID)
                ->atPath('birthday')
                ->addViolation();
        }
    }
}