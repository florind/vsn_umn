<?php
namespace App\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use App\Helper\ApiMessage;

class PasswordResetRequest
{
    /**
     * @var string
     * @Assert\NotBlank(message=ApiMessage::ERROR_EMPTY)
     * @Assert\Regex(
     *     pattern = "/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/",
     *     htmlPattern = false,
     *     message=ApiMessage::ERROR_VALUE_INVALID
     * )
     */
    private $password;

    /**
     * @var string
     * @Assert\NotBlank(message=ApiMessage::ERROR_EMPTY)
     * @Assert\Email(
     *     checkMX=true,
     *     message=ApiMessage::ERROR_VALUE_INVALID
     * )
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(message=ApiMessage::ERROR_EMPTY)
     */
    private $token;

    private $locale;

    public function __construct(Request $request)
    {
        $this->email = $request->get('email', null);
        $this->password = $request->get('pass', null);
        $this->token = $request->get('token', null);
        $this->locale = $request->get('locale', 'en');
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

}