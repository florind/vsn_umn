<?php
namespace App\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use App\Helper\ApiMessage;

class AuthenticateRequest
{
    /**
     * @var string
     * @Assert\NotBlank(message=ApiMessage::ERROR_EMPTY)
     * @Assert\Email(
     *     checkMX=true,
     *     message=ApiMessage::ERROR_VALUE_INVALID
     * )
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(message=ApiMessage::ERROR_EMPTY)
     */
    private $password;

    public function __construct(Request $request)
    {
        $this->email = $request->get('email', null);
        $this->password = $request->get('pass', null);
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}