<?php
namespace App\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use App\Helper\ApiMessage;

class ActivateUserRequest
{
    /**
     * @var string
     * @Assert\NotBlank(message=ApiMessage::ERROR_EMPTY)
     */
    private $token;

    /**
     * @var string
     * @Assert\NotBlank(message=ApiMessage::ERROR_EMPTY)
     * @Assert\Email(
     *     checkMX=true,
     *     message=ApiMessage::ERROR_VALUE_INVALID
     * )
     */
    private $email;

    private $locale;

    public function __construct(Request $request)
    {
        $this->token = $request->get('token', null);
        $this->email = $request->get('email', null);
        $this->locale = $request->get('locale', 'en');
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    public function getEmail()
    {
        return $this->email;
    }

}