<?php
namespace App\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use App\Helper\ApiMessage;

class GenerateActivationCodeRequest
{
    /**
     * @var string
     * @Assert\NotBlank(message=ApiMessage::ERROR_EMPTY)
     * @Assert\Email(
     *     checkMX=true,
     *     message=ApiMessage::ERROR_VALUE_INVALID
     * )
     */
    private $email;

    private $locale;

    public function __construct(Request $request)
    {
        $this->email = $request->get('email', null);
        $this->locale = $request->get('locale', 'en');
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

}