<?php
namespace App\Controller;

use App\Exception\AccessDeniedException;
use App\Exception\InvalidTokenException;
use App\Helper\ApiMessage;
use App\Request\ActivateUserRequest;
use App\Request\AuthenticateRequest;
use App\Request\GenerateActivationCodeRequest;
use App\Request\IdentityTokenRequest;
use App\Request\ForgotPasswordRequest;
use App\Request\PasswordResetRequest;
use App\Request\RegisterRequest;
use App\Response\EmptyResponse;
use App\Response\ValidationErrorResponse;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SecurityController extends ApiController
{
    public function register(
        Request $request,
        ValidatorInterface $validator,
        UserService $userService
    ) {
        $model = new RegisterRequest($request);
        $errors = $validator->validate($model);

        if ($errors->count()) {
            $validationErrors = [];

            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $validationErrors[$error->getPropertyPath()] = $error->getMessage();
            }

            return new JsonResponse(
                new ValidationErrorResponse($validationErrors),
                Response::HTTP_BAD_REQUEST
            );
        }

        $userService->register($model);

        return new JsonResponse(new EmptyResponse(ApiMessage::RESPONSE_SUCCESS));
    }

    public function authenticate(
        Request $request,
        UserService $userService,
        ValidatorInterface $validator
    ) {
        $model = new AuthenticateRequest($request);
        $errors = $validator->validate($model);

        if ($errors->count()) {
            throw new AccessDeniedException();
        }

        $authenticationResponse = $userService->authenticate($model);

        return new JsonResponse($authenticationResponse);
    }

    public function generateIdentityToken(
        Request $request,
        UserService $userService,
        ValidatorInterface $validator
    ) {
        $model = new IdentityTokenRequest($request);
        $errors = $validator->validate($model);

        if ($errors->count()) {
            return new JsonResponse(
                new EmptyResponse(ApiMessage::RESPONSE_VALIDATION_ERROR),
                Response::HTTP_BAD_REQUEST
            );
        }

        $response = $userService->generateIdentityToken($model);
        return new JsonResponse($response);
    }

    public function activateUser(
        Request $request,
        UserService $userService,
        ValidatorInterface $validator
    ) {
        $model = new ActivateUserRequest($request);
        $errors = $validator->validate($model);

        if ($errors->count()) {
            return new JsonResponse(
                new EmptyResponse(ApiMessage::RESPONSE_VALIDATION_ERROR),
                Response::HTTP_BAD_REQUEST
            );
        }

        try {
            $userService->activate($model);
        } catch (InvalidTokenException $exception) {
            return new JsonResponse(
                new EmptyResponse(ApiMessage::RESPONSE_INVALID_TOKEN),
                Response::HTTP_BAD_REQUEST
            );
        }

        return new JsonResponse(
            new EmptyResponse(ApiMessage::RESPONSE_SUCCESS),
            Response::HTTP_OK
        );
    }

    public function generateActivationCode(
        Request $request,
        UserService $userService,
        ValidatorInterface $validator
    ) {
        $model = new GenerateActivationCodeRequest($request);
        $errors = $validator->validate($model);

        if ($errors->count()) {
            return new JsonResponse(
                new EmptyResponse(ApiMessage::RESPONSE_VALIDATION_ERROR),
                Response::HTTP_BAD_REQUEST
            );
        }

        $userService->resendActivationCode($model);
        return new JsonResponse(
            new EmptyResponse(ApiMessage::RESPONSE_SUCCESS),
            Response::HTTP_OK
        );
    }

    public function forgotPassword(
        Request $request,
        UserService $userService,
        ValidatorInterface $validator
    ) {
        $model = new ForgotPasswordRequest($request);
        $errors = $validator->validate($model);

        if ($errors->count()) {
            return new JsonResponse(
                new EmptyResponse(ApiMessage::RESPONSE_VALIDATION_ERROR),
                Response::HTTP_BAD_REQUEST
            );
        }

        $userService->forgotPassword($model);

        return new JsonResponse(
            new EmptyResponse(ApiMessage::RESPONSE_SUCCESS),
            Response::HTTP_OK
        );
    }

    public function resetPassword(
        Request $request,
        ValidatorInterface $validator,
        UserService $userService
    ) {
        $model = new PasswordResetRequest($request);
        $violations = $validator->validate($model);

        if ($violations->count()) {
            $errors = [];

            /** @var ConstraintViolation $violation */
            foreach ($violations as $violation) {
                $errors[$violation->getPropertyPath()] = $violation->getMessage();
            }

            return new JsonResponse(
                new ValidationErrorResponse($errors),
                Response::HTTP_BAD_REQUEST
            );
        }

        $userService->resetPassword($model);

        return new JsonResponse(
            new EmptyResponse(ApiMessage::RESPONSE_SUCCESS),
            Response::HTTP_OK
        );
    }
}