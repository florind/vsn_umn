<?php
namespace App\EventSubscriber;

use App\Controller\ApiController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class AuthorizeClientSubscriber implements EventSubscriberInterface
{
    private $accessKey;

    public function __construct(string $accessKey)
    {
        $this->accessKey = $accessKey;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'authorizeRequest'
        ];
    }

    public function authorizeRequest(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if ($controller[0] instanceof ApiController) {
            $token = $event->getRequest()->headers->get('X-AUTH-TOKEN', null);

            if (empty($token) || $this->accessKey !== $token) {
                throw new NotFoundHttpException();
            }
        }
    }
}