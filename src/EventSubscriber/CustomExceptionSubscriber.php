<?php
namespace App\EventSubscriber;

use App\Controller\ApiController;
use App\Exception\ApiException;
use App\Exception\UserExistsException;
use App\Response\EmptyResponse;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class CustomExceptionSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'exceptionJsonResponse'
        ];
    }

    public function exceptionJsonResponse(GetResponseForExceptionEvent $event)
    {
        $ex = $event->getException();

        if ($ex instanceof ApiException) {
            $event->setResponse(new JsonResponse(
                new EmptyResponse($ex->getMessage()),
                $ex->getCode()
            ));
        } else {
            $event->setResponse(new JsonResponse(null, Response::HTTP_NOT_FOUND));
        }
    }
}