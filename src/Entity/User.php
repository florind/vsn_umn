<?php
namespace App\Entity;

use App\Exception\AccessDeniedException;
use App\Exception\InvalidTokenException;
use App\Exception\PasswordResetException;
use App\Exception\TokenLockedException;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    /** @var  int */
    private $id;

    /** @var  string */
    private $email;

    /** @var  string */
    private $password;

    /** @var  string */
    private $refreshToken;

    /** @var  Status */
    private $status;

    /** @var  Token */
    private $token;

    /** @var  int */
    private $profileId;

    public static function build(string $email) {
        $user = new self(
            $email,
            Status::locked(),
            Token::forAccountActivation($email)
        );

        $user->generateRefreshToken();

        return $user;
    }

    private function __construct(
        string $email,
        Status $status,
        Token $token
    ) {
        $this->email = $email;
        $this->status = $status;
        $this->token = $token;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt(): string
    {
        return '';
    }

    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }

    public function generateRefreshToken(): self
    {
        $salt = bin2hex(random_bytes(10));

        $this->refreshToken = hash(
            'sha256',
            sprintf("%s{%s}%s", $this->email, $salt, microtime(true))
        );

        return $this;
    }

    public function lock(): self
    {
        $this->status = Status::locked();
        return $this;
    }

    public function isLocked(): bool
    {
        return $this->status->isLocked();
    }

    public function isActive(): bool
    {
        return $this->status->isActive();
    }

    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    public function getUsername(): string
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function refreshActivationToken()
    {
        if (
            !$this->isLocked() ||
            null === $this->token ||
            !$this->token->isActivationToken()
        ) {
            throw new AccessDeniedException();
        }

        $this->token = Token::forAccountActivation($this->email);

        return $this;
    }

    public function activate($code)
    {
        if (!$this->isLocked()) {
            throw new InvalidTokenException();
        }

        $this->token->attemptActivation($code, $this->email);
        $this->token = Token::null();
        $this->status = Status::active();
        return $this;

    }

    public function forgotPassword()
    {
        if (!$this->isActive()) {
            throw new PasswordResetException();
        }

        $this->token = Token::forPasswordReset($this->email);
    }

    public function resetPassword(string $code, string $newPassword)
    {
        if (!$this->isActive()) {
            throw new InvalidTokenException();
        }

        try {
            $this->token->attemptReset($code, $this->email);
            $this->token = Token::null();
            $this->password = $newPassword;
            $this->generateRefreshToken();
        } catch (InvalidTokenException $ex) {
            if ($this->token->exhaustedResetAttempts()) {
                $this->clearToken();
                throw new TokenLockedException();
            }
            throw $ex;
        }
    }

    public function getToken()
    {
        return $this->token;
    }

    private function clearToken()
    {
        $this->token = Token::null();
    }

    public function getProfileId(): ?string
    {
        return $this->profileId;
    }

    public function setProfileId(string $profileId): User
    {
        $this->profileId = $profileId;
        return $this;
    }

}