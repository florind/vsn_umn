<?php
namespace App\Entity;

use App\Exception\InvalidTokenException;

class Token
{
    const RANGE_MIN = 100000;
    const RANGE_MAX = 999999;

    const RESET_MAX_RETRIES = 3;
    const ACTIVATION_MAX_RETRIES = 0;

    const SCOPE_PASSWORD_RESET = 'reset';
    const SCOPE_ACCOUNT_ACTIVATION = 'activate';

    /** @var string */
    private $code;

    /** @var int */
    private $plainCode;

    /** @var string */
    private $scope;

    /** @var  \DateTime */
    private $expires;

    /** @var  int */
    private $attempts;

    public static function null()
    {
        return new self();
    }

    public static function forAccountActivation(string $email)
    {
        return new self(self::SCOPE_ACCOUNT_ACTIVATION, new \DateInterval('P30D'), $email);
    }

    public static function forPasswordReset(string $email)
    {
        return new self(self::SCOPE_PASSWORD_RESET, new \DateInterval('PT1H'), $email);
    }

    private function __construct(string $codeScope = null, \DateInterval $validInterval = null, $email = null)
    {
        if (null === $codeScope && null === $validInterval) {
            return;
        }

        $this->scope = $codeScope;
        $this->plainCode = mt_rand(self::RANGE_MIN, self::RANGE_MAX);
        $this->code = $this->hashCode($this->plainCode, $email);
        $this->expires = (new \DateTime())->add($validInterval);
        $this->attempts = 0;
    }

    public function attemptActivation($code, string $email)
    {
        if (
            $this->code !== $this->hashCode($code, $email) ||
            (new \DateTime()) >= $this->expires ||
            $this->scope !== self::SCOPE_ACCOUNT_ACTIVATION
        ) {
            $this->attempts++;
            throw new InvalidTokenException();
        }
    }

    public function attemptReset(string $code, string $email)
    {
        if (
            $this->attempts >= self::RESET_MAX_RETRIES ||
            $this->code !== $this->hashCode($code, $email) ||
            (new \DateTime()) >= $this->expires ||
            $this->scope !== self::SCOPE_PASSWORD_RESET
        ) {
            $this->attempts++;
            throw new InvalidTokenException();
        }
    }

    public function getPlainCode()
    {
        if (null === $this->plainCode) {
            throw new \Exception('The plain code can only be retrieved at the time of the creation!');
        }

        return $this->plainCode;
    }

    private function hashCode(string $code, string $email)
    {
        return hash('sha256', sprintf("%s{%s}", $code, $email));
    }

    public function exhaustedResetAttempts()
    {
        return $this->attempts >= self::RESET_MAX_RETRIES;
    }

    public function isActivationToken()
    {
        return self::SCOPE_ACCOUNT_ACTIVATION === $this->scope;
    }
}