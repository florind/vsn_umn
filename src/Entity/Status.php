<?php
namespace App\Entity;

class Status
{
    const STATUS_ACTIVE = 'active';
    const STATUS_LOCKED = 'locked';

    private $status;

    private function __construct(string $status)
    {
        $this->status = $status;
    }

    public static function active()
    {
        return new self(self::STATUS_ACTIVE);
    }

    public static function locked()
    {
        return new self(self::STATUS_LOCKED);
    }

    public function isLocked()
    {
        return $this->status === self::STATUS_LOCKED;
    }

    public function isActive()
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function value()
    {
        return $this->status;
    }

    public function __toString()
    {
        return $this->value();
    }


}