<?php
namespace App\DomainEvent;

use Psr\Log\InvalidArgumentException;

class UserRegisteredEvent implements DomainEventInterface
{
    const KEY = 'user.registered';

    private $id;
    private $firstName;
    private $lastName;
    private $birthday;
    private $gender;
    private $locale;
    private $token;

    public static function fromJson(string $json)
    {
        $data = json_decode($json, true);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new InvalidArgumentException('Invalid JSON');
        }

        if (
            !array_key_exists('id', $data) ||
            !array_key_exists('first_name', $data) ||
            !array_key_exists('last_name', $data) ||
            !array_key_exists('birthday', $data) ||
            !array_key_exists('gender', $data) ||
            !array_key_exists('locale', $data) ||
            !array_key_exists('token', $data)
        ) {
            throw new InvalidArgumentException('JSON is missing some fields');
        }

        return new self(
            $data['id'],
            $data['first_name'],
            $data['last_name'],
            $data['birthday'],
            $data['gender'],
            $data['locale'],
            $data['token']
        );
    }

    public function __construct(
        int $id,
        string $firstName,
        string $lastName,
        string $birthday,
        string $gender,
        string $locale,
        string $token
    ) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->birthday = $birthday;
        $this->gender = $gender;
        $this->locale = $locale;
        $this->token = $token;
    }

    public function getKey(): string
    {
        return self::KEY;
    }

    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'birthday' => $this->birthday,
            'gender' => $this->gender,
            'locale' => $this->locale,
            'token' => $this->token
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getBirthday(): string
    {
        return $this->birthday;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function getToken(): string
    {
        return $this->token;
    }

}