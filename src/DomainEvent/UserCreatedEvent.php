<?php
namespace App\DomainEvent;

class UserCreatedEvent implements DomainEventInterface
{
    const KEY = 'user.created';

    private $email;
    private $activationToken;
    private $locale;

    public function __construct(string $email, string $activationToken, string $locale)
    {
        $this->email = $email;
        $this->locale = $locale;
        $this->activationToken = $activationToken;
    }

    public function getKey(): string
    {
        return self::KEY;
    }

    function jsonSerialize()
    {
        return [
            'locale' => $this->locale,
            'email' => $this->email,
            'activation_token' => $this->activationToken
        ];
    }
}