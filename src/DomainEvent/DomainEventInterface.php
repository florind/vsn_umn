<?php
namespace App\DomainEvent;

interface DomainEventInterface extends \JsonSerializable
{
    public function getKey(): string;
}