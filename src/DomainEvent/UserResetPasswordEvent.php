<?php
namespace App\DomainEvent;

class UserResetPasswordEvent implements DomainEventInterface
{
    const KEY = 'user.reset_password';

    private $email;

    private $locale;

    public function __construct(string $email, string $locale)
    {
        $this->email = $email;
        $this->locale = $locale;
    }

    public function getKey(): string
    {
        return self::KEY;
    }

    function jsonSerialize()
    {
        return [
            'email' => $this->email,
            'locale' => $this->locale
        ];
    }
}