<?php
namespace App\DomainEvent;

use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;

class DomainEventPublisher
{
    private $producer;

    public function __construct(ProducerInterface $producer)
    {
        $this->producer = $producer;
    }

    public function publish(DomainEventInterface $event)
    {
        $this->producer->publish(json_encode($event), $event->getKey());
    }
}