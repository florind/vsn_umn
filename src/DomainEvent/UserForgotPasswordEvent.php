<?php
namespace App\DomainEvent;

class UserForgotPasswordEvent implements DomainEventInterface
{
    const KEY = 'user.forgot_password';

    private $email;
    private $locale;
    private $resetToken;

    public function __construct(
        string $email,
        string $resetToken,
        string $locale
    ) {
        $this->email = $email;
        $this->locale = $locale;
        $this->resetToken = $resetToken;
    }

    public function getKey(): string
    {
        return self::KEY;
    }

    function jsonSerialize()
    {
        return [
            'email' => $this->email,
            'locale' => $this->locale,
            'reset_token' => $this->resetToken
        ];
    }
}